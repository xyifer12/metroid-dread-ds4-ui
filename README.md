Tested on Ryujinx with game version 1.0.0 NTSC.
Blue lines are from this mod: https://gamebanana.com/mods/330216
Some buttons are straight from https://thoseawesomeguys.com/prompts/ and some are edited.
DS4 images are from Bing Images.

![](Preview.png)
